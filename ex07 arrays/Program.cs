﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ex07_arrays
{
    public class Program
    {
        static void Main(string[] args)
        {
            /*      Øvelse 2.2      */
            Console.WriteLine("Øvelse 2.2");

            // Definer vores variabler
            double michelAlder;
            double benjaminAlder;
            double andreasAlder;
            double kristofferAlder;
            double christianAlder;
            double gennemsnitsAlder;

            // Initialiser vores variabler
            michelAlder = 25.0;
            benjaminAlder = 21.0;
            kristofferAlder = 20.0;
            andreasAlder = 24.0;
            christianAlder = 26.0;
            gennemsnitsAlder = 0.0;

            //Print aldre
            Console.WriteLine(michelAlder);
            Console.WriteLine(benjaminAlder);
            Console.WriteLine(andreasAlder);
            Console.WriteLine(kristofferAlder);
            Console.WriteLine(christianAlder);

            //Beregn gennemsnitsalderen
            gennemsnitsAlder = (michelAlder + benjaminAlder + kristofferAlder + andreasAlder + christianAlder) / 5;

            //Print gennemsnits alderen
            Console.WriteLine(gennemsnitsAlder);

            Console.ReadKey();

            Console.Clear();

            /* Øvelse 2.3 */
            Console.WriteLine("Øvelse 2.3");

            //Definer variabler
            double[] aldersArray = new double[5];

            //Fyld arrayet med aldre
            aldersArray[0] = michelAlder; 
            aldersArray[1] = benjaminAlder; 
            aldersArray[2] = andreasAlder; 
            aldersArray[3] = kristofferAlder; 
            aldersArray[4] = christianAlder;

            //Sæt gennemsnitsalder til 0
            gennemsnitsAlder = 0.0;

            //Udregn gennemsnit udfra vores array

            for(int index = 0; index < 5; index = index + 1)
            {
                Console.WriteLine(aldersArray[index]);
                gennemsnitsAlder = gennemsnitsAlder + aldersArray[index];
            }

            //Nu har vi samlet alle aldre i gennemsnitsAlder variablen - vi skal dividere med antal personer for at få
            //gennemsnittet
            gennemsnitsAlder = gennemsnitsAlder / 5;

            Console.WriteLine(gennemsnitsAlder);

            Console.ReadKey();

            Console.Clear();

            /* Øvelse 2.4 */
            Console.WriteLine("Øvelse 2.4");

            //Køre alle sokkepar igennem
            for (int index = 0; index < 5; index = index + 1)
            {
                if(aldersArray[index] == 21.0)
                {
                    Console.WriteLine("Alderen eksistere!");
                    break;
                }
            }

            Console.ReadKey();
        }

        public static int CandleBlower(int[] arr)
        {
            int højesteNummer;
            int antal;

            højesteNummer = 0;
            antal = 0;

            for(int i = 0; i < arr.Length; i++)
            {
                if (arr[i] > højesteNummer)
                {
                    antal = 0;
                    højesteNummer = arr[i];
                }

                if (arr[i] == højesteNummer)
                    antal = antal + 1;
            }

            return antal;
        }

        public static int SockMerchant(int[] arr)
        {
            /* Løsning med list */
            int par = 0;
            var sokkeliste = arr.ToList<int>();

            while (sokkeliste.Count > 1)
            {
                int sok = sokkeliste[0];
                sokkeliste.RemoveAt(0);

                for (int index = 0; index < sokkeliste.Count; index++)
                {
                    if (sokkeliste[index] == sok)
                    {
                        par = par + 1;
                        sokkeliste.RemoveAt(index);
                        break;
                    }
                }
            }
            return par;



            /*      Løsning med kun arrays
             *  Vi har et array med X antal sokker
             *  
             */

            //definer variabler
            //int par;
            //int sok1;
            //int sok2;
            //bool[] brugteSokker;
            //
            //// initialser
            //par = 0;
            //sok1 = 0;
            //sok2 = 0;
            //brugteSokker = new bool[arr.Length];
            //
            //for(int sokindex1 = 0; sokindex1 < arr.Length; sokindex1 = sokindex1 + 1)
            //{
            //   if (brugteSokker[sokindex1] == true)
            //       continue;
            //
            //    sok1 = arr[sokindex1];
            //    brugteSokker[sokindex1] = true;
            //
            //    for (int sokindex2 = 0; sokindex2 < arr.Length; sokindex2 = sokindex2 + 1)
            //    {
            //        if(sokindex2 == sokindex1)
            //            continue;
            //
            //        if (brugteSokker[sokindex2] == true)
            //            continue;
            //
            //        sok2 = arr[sokindex2];
            //
            //        if(sok1 == sok2)
            //        {
            //            brugteSokker[sokindex2] = true;
            //            par = par + 1;
            //            break;
            //        }
            //    }
            //}
            //
            //return par;
        }
    }
}
